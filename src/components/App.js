import React from "react";
import marked from "marked";

import "./styles/App.css";

marked.setOptions({
  breaks: true,
  gfm: true,
  headerIds: false
});

const renderer = new marked.Renderer();

let initialValue = `# Esto es un título h1
## Esto es un título h2

Esto es un enlace [React](https://reactjs.org/)

La siguiente línea es un código en línea
\`$ ssh -t 192.168.1.1 'code'\`

El siguiente es un código en bloque
\`\`\`
  function sayHello(){
    alert('Hello!!')
  }
\`\`\`

Ahora una lista desordenada
- Esto es un item de lista
- Este es un segundo item de lista

Y otra ordenada
1. Primero
2. Segundo

Esto es un bloque de cita
> Nunca pares de intentar, aunque te digan que es imposible.

Lo siguiente es una imagen
![Este sería el subtítulo](https://www.spacex.com/sites/spacex/files/styles/media_gallery_thumbnail/public/first_reflight_-_12_dsc_5635.jpg?itok=HXbyo6IK "World S First Re Flown Orbital Class Booster Arrives In Port")

**Esto es un texto en negrita**
`;

class App extends React.Component {
  state = {
    input: initialValue
  };

  handleChange = e => {
    this.setState({
      input: e.target.value
    });
  };

  render() {
    // const textArea = document.querySelector("textarea");
    // const textRowCount = textArea ? textArea.value.split("\n").length : 0;
    // const rows = textRowCount + 1;

    return (
      <div className='container-fluid p-0'>
        <main className='row no-gutters'>
          <article className='col-6'>
            <textarea
              id='editor'
              className='p-4'
              onChange={this.handleChange}
              //   rows={rows}
              value={this.state.input}
            />
          </article>
          <article className='col-6'>
            <div
              id='preview'
              className='p-4'
              dangerouslySetInnerHTML={{
                __html: marked(this.state.input, { renderer: renderer })
              }}
            />
          </article>
        </main>
      </div>
    );
  }
}

export default App;
